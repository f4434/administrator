package com.devups.administrator.controller;

import java.util.List;
import java.util.Optional;

import com.devups.administrator.core.OrdensCore;
import com.devups.administrator.model.OrdensEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/administrator")
public class OrdensController {
    
    @Autowired
    OrdensCore ordensCore;

    @GetMapping("/todo")
    List<OrdensEntity> encuentraTodo(){
        return ordensCore.findAll();
    }
    
    @PostMapping("/crear")
    OrdensEntity crearOrden(@RequestBody OrdensEntity ordensEntity){
        return ordensCore.create(ordensEntity);

    }
    @PostMapping("/actualizar")
    Optional<OrdensEntity> actualizarOrden(@RequestBody OrdensEntity ordensEntity){
        return ordensCore.update(ordensEntity);
    }
    @DeleteMapping("/{id}")
    Boolean borrarOrden(@PathVariable(value = "id") Long id){
        return ordensCore.delete(id);
    }

    @GetMapping("/by/{id}")
    List<OrdensEntity> encontrarOrdenNumero(@PathVariable(value = "id") String numeroOrden){
        return ordensCore.findByOrden(numeroOrden);

    }
}
