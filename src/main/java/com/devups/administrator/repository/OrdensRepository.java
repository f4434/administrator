package com.devups.administrator.repository;

import com.devups.administrator.model.HistoryOrdenEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrdensRepository extends CrudRepository<HistoryOrdenEntity, Long>{
    
}
